use pdf_extract::extract_text;
use regex::Captures;
use regex::Regex;
use std::env;
use std::fs;
use std::path::Path;

fn main() {
    let out_dir = env::var_os("OUT_DIR").unwrap();

    let ocpp_1_6_specification_txt_path = Path::new(&out_dir).join("OCPP-1.6.txt");
    let ocpp_1_6_specification = if !ocpp_1_6_specification_txt_path.exists() {
        let ocpp_1_6_specification_pdf_path = Path::new("docs")
            .join("OCPP")
            .join("OCPP_1.6_documentation_2019_12")
            .join("ocpp-1.6 edition 2.pdf");
        let ocpp_1_6_specification = extract_text(ocpp_1_6_specification_pdf_path).unwrap();
        fs::write(&ocpp_1_6_specification_txt_path, &ocpp_1_6_specification).unwrap();
        ocpp_1_6_specification
    } else {
        fs::read_to_string(ocpp_1_6_specification_txt_path).unwrap()
    };

    // 2.0.1
    let ocpp_2_0_1_specification_txt_path = Path::new(&out_dir).join("OCPP-2.0.1.txt");
    let ocpp_2_0_1_specification = if !ocpp_2_0_1_specification_txt_path.exists() {
        let ocpp_2_0_1_specification_pdf_path = Path::new("docs")
            .join("OCPP")
            .join("OCPP-2.0.1-Specification")
            .join("OCPP-2.0.1_part2_specification.pdf");
        let ocpp_2_0_1_specification = extract_text(ocpp_2_0_1_specification_pdf_path).unwrap();
        fs::write(
            &ocpp_2_0_1_specification_txt_path,
            &ocpp_2_0_1_specification,
        )
        .unwrap();
        ocpp_2_0_1_specification
    } else {
        fs::read_to_string(ocpp_2_0_1_specification_txt_path).unwrap()
    };

    // concatenate splitted long lines
    let ocpp_2_0_1_specification = Regex::new(r"(?m)(.)\n(.)")
        .unwrap()
        .replace_all(&ocpp_2_0_1_specification, |captures: &Captures| {
            format!("{}{}", &captures[1], &captures[2])
        });
    // remove empty lines following a line with text
    let ocpp_2_0_1_specification = Regex::new(r"(?m)^(.+)\n\n")
        .unwrap()
        .replace_all(&ocpp_2_0_1_specification, |captures: &Captures| {
            format!("{}\n", &captures[1])
        });
    // delete page headers
    let ocpp_2_0_1_specification = Regex::new(r"(?m)^FINAL, 2020-03-31.*\n")
        .unwrap()
        .replace_all(&ocpp_2_0_1_specification, "");
    // delete page footers
    let ocpp_2_0_1_specification = Regex::new(
        r"(?m)^OCPP 2.0.1 - © Open Charge Alliance 2020 \d*/\d* Part 2 - Specification\n",
    )
    .unwrap()
    .replace_all(&ocpp_2_0_1_specification, "");

    // cut the first part before the messages
    let ocpp_2_0_1_messages = Regex::new(r"(?m)(.|\n)*^1\.\sMessages\n")
        .unwrap()
        .replace(&ocpp_2_0_1_specification, "");
    // cut the first part before the datatypes
    let ocpp_2_0_1_datatypes = Regex::new(r"(?m)(.|\n)*^2\.\sDatatypes\n")
        .unwrap()
        .replace(&ocpp_2_0_1_messages, "");
    // cut the part after the messages
    let ocpp_2_0_1_messages = Regex::new(r"(?m)^2\.\sDatatypes\n(.|\n)*")
        .unwrap()
        .replace(&ocpp_2_0_1_messages, "");
    // cut the first part before the enumerations
    let ocpp_2_0_1_enumerations = Regex::new(r"(?m)(.|\n)*^3\.\sEnumerations\n")
        .unwrap()
        .replace(&ocpp_2_0_1_datatypes, "");
    // cut the part after the datatypes
    let ocpp_2_0_1_datatypes = Regex::new(r"(?m)^3\.\sEnumerations\n(.|\n)*")
        .unwrap()
        .replace(&ocpp_2_0_1_datatypes, "");
    // cut the part after the enumerations
    let ocpp_2_0_1_enumerations = Regex::new(r"(?m)^Referenced Components and Variables\n(.|\n)*")
        .unwrap()
        .replace(&ocpp_2_0_1_enumerations, "");

    // enumerations
    let enumerations_txt_path = Path::new(&out_dir).join("enumerations.txt");
    fs::write(&enumerations_txt_path, ocpp_2_0_1_enumerations.as_ref()).unwrap();
    let ocpp_2_0_1_enumerations = convert_ocpp_2_0_1_txt(
        ocpp_2_0_1_enumerations.as_ref(),
        r"(?ms)^(.*?)\n^Enumeration\n(.*?)^Value Description\n(.*)",
        Some(process_enums_inner_data),
        enums_to_code,
        enums_map_attribute,
    );
    println!("{:?}", ocpp_2_0_1_enumerations);
    let enumerations_path = Path::new(&out_dir).join("enumerations.rs");
    fs::write(
        &enumerations_path,
        ocpp_2_0_1_enumerations
            .iter()
            .map(|(name, code)| code.as_str())
            .collect::<Vec<&str>>()
            .join("\n"),
    )
    .unwrap();

    // messages
    let messages_txt_path = Path::new(&out_dir).join("messages.txt");
    fs::write(&messages_txt_path, ocpp_2_0_1_messages.as_ref()).unwrap();
    let ocpp_2_0_1_messages = convert_ocpp_2_0_1_txt(
        ocpp_2_0_1_datatypes.as_ref(),
        r"(?ms)^(.*?)\n^Class\n(.*?)^Field Name Field Type Card. Description\n(.*)",
        Some(process_messages_inner_data),
        messages_to_code,
        messages_map_attribute,
    );
    println!("{:?}", ocpp_2_0_1_messages);
    let messages_path = Path::new(&out_dir).join("messages.rs");
    fs::write(
        &messages_path,
        ocpp_2_0_1_messages
            .iter()
            .map(|(name, code)| code.as_str())
            .collect::<Vec<&str>>()
            .join("\n"),
    )
    .unwrap();

    // datatypes
    let datatypes_txt_path = Path::new(&out_dir).join("datatypes.txt");
    fs::write(&datatypes_txt_path, ocpp_2_0_1_datatypes.as_ref()).unwrap();
    let ocpp_2_0_1_datatypes = convert_ocpp_2_0_1_txt(
        ocpp_2_0_1_datatypes.as_ref(),
        r"(?ms)^(.*?)\n^Class\n(.*?)^Field Name Field Type Card. Description\n(.*)",
        Some(process_datatypes_inner_data),
        datatypes_to_code,
        datatypes_map_attribute,
    );
    println!("{:?}", ocpp_2_0_1_datatypes);
    let datatypes_path = Path::new(&out_dir).join("datatypes.rs");
    fs::write(&datatypes_path, primitive_datatypes()).unwrap();
    fs::write(
        &datatypes_path,
        primitive_datatypes()
            + &ocpp_2_0_1_datatypes
                .iter()
                .map(|(name, code)| code.as_str())
                .collect::<Vec<&str>>()
                .join("\n"),
    )
    .unwrap();

    println!("cargo:rerun-if-changed=build.rs");
}

// Converts the types (enumerations and classes) from 1.6 spec pdf into array of name, code tuple
fn convert_ocpp_1_6_types(ocpp_2_0_1_enumerations: &str) -> Vec<(String, String)> {
    let split_string = "QPWOEIRUTYALSKDJFHGZMXNCBV".to_owned();
    let split_regex = Regex::new(&split_string).unwrap();
    let enum_spec_split_regex =
        Regex::new(r"(?ms)^(.*?)\n^Enumeration\n(.*?)^Value Description\n(.*)").unwrap();
    let new_line_regex = Regex::new("\n").unwrap();

    //let enum_begin_regex = Regex::new(r"(?m)^\d*\.\d*\.\s([A-Za-z0-9]*)\n").unwrap();
    let enum_begin_regex = Regex::new(r"(?m)^(?:\d*\.\d*\.\s)").unwrap();
    //println!("values = {}\n", &ocpp_2_0_1_enumerations[0..1000]);
    //let values: Vec<_> = enum_begin_regex.split(ocpp_2_0_1_enumerations).next().into_iter().collect();
    let mut enums = enum_begin_regex
        .split(ocpp_2_0_1_enumerations)
        .map(|enum_spec| {
            let enum_spec = enum_spec_split_regex.replace_all(&enum_spec, |captures: &Captures| {
                let key = captures[1].to_owned();
                let name = captures[1].to_owned();
                let comments = captures[2]
                    .split("\n")
                    .map(|comment| {
                        if comment.eq("") {
                            format!("")
                        } else {
                            format!("\t// {}", comment)
                        }
                    })
                    .collect::<Vec<String>>()
                    .join("\n");
                let values = captures[3]
                    .split("\n")
                    .map(|value| {
                        if value.eq("") {
                            format!("")
                        } else {
                            let (value, comment) = value.split_once(" ").unwrap_or((value, ""));
                            let key = value.replace("-", "_").replace(".", "_");
                            format!("\t{} = \"{}\", // {}", key, value, comment)
                        }
                    })
                    .collect::<Vec<String>>()
                    .join("\n");
                format!(
                    "{}{}{}{}{}{}{}",
                    key, split_string, name, split_string, comments, split_string, values
                )
            });
            let enum_spec = split_regex
                .split(&enum_spec)
                .map(|s| s.to_owned())
                .collect::<Vec<String>>();
            let key = enum_spec[0].to_owned();
            let name = if enum_spec.len() > 1 {
                enum_spec[1].to_owned()
            } else {
                "".to_owned()
            };
            let comments = if enum_spec.len() > 2 {
                enum_spec[2].to_owned()
            } else {
                "".to_owned()
            };
            let values = if enum_spec.len() > 3 {
                enum_spec[3].to_owned()
            } else {
                "".to_owned()
            };
            (key, name, comments, values)
        })
        .collect::<Vec<(String, String, String, String)>>();
    enums.remove(0);

    let enums = enums
        .iter()
        .map(|(key, name, comments, values)| {
            (
                name.to_owned(),
                format!(
                    "/* {} */\npub enum {} {{\n{}\n{}}}\n",
                    key, name, comments, values
                ),
            )
        })
        .collect::<Vec<(String, String)>>();
    //println!("{}", enums.join("\n"));

    enums
}

// convert enumerations, datatypes, messages txt files taken from pdf to text conversion
fn convert_ocpp_2_0_1_txt<PID, TC, MA>(
    ocpp_2_0_1_txt: &str,
    data_spec_split_regex: &str,
    process_inner_data: Option<PID>,
    to_code: TC,
    map_attribute: MA,
) -> Vec<(String, String)>
where
    PID: Fn(String, String, String, String) -> (String, String, String, String),
    TC: Fn(String, String, String, String) -> (String, String),
    MA: Fn(&str) -> String,
{
    let split_string = "QPWOEIRUTYALSKDJFHGZMXNCBV".to_owned();
    let split_regex = Regex::new(&split_string).unwrap();
    let data_spec_split_regex = Regex::new(data_spec_split_regex).unwrap();
    //let new_line_regex = Regex::new("\n").unwrap();
    let data_begin_regex = Regex::new(r"(?m)^(?:\d*\.\d*\.\s)").unwrap();

    let mut data_entries = data_begin_regex
        .split(ocpp_2_0_1_txt)
        .map(|data_spec| {
            let data_spec = data_spec_split_regex.replace_all(&data_spec, |captures: &Captures| {
                println!("AAAAAAAA\n{:#?}\nBBBBBBBB", captures);
                let key = captures[1].to_owned();
                let name = captures[1].to_owned();
                let comments = captures[2]
                    .split("\n")
                    .map(|comment| {
                        if comment.eq("") {
                            format!("")
                        } else {
                            format!("\t// {}", comment)
                        }
                    })
                    .collect::<Vec<String>>()
                    .join("\n");
                let attributes = captures[3]
                    .split("\n")
                    .map(|attribute| map_attribute(attribute))
                    .collect::<Vec<String>>()
                    .join("\n");
                format!(
                    "{}{}{}{}{}{}{}",
                    key, split_string, name, split_string, comments, split_string, attributes
                )
            });
            let data_spec = split_regex
                .split(&data_spec)
                .map(|s| s.to_owned())
                .collect::<Vec<String>>();
            let key = data_spec[0].to_owned();
            let name = if data_spec.len() > 1 {
                data_spec[1].to_owned()
            } else {
                "".to_owned()
            };
            let comments = if data_spec.len() > 2 {
                data_spec[2].to_owned()
            } else {
                "".to_owned()
            };
            let values = if data_spec.len() > 3 {
                data_spec[3].to_owned()
            } else {
                "".to_owned()
            };

            if let Some(process_inner_data) = &process_inner_data {
                process_inner_data(key, name, comments, values)
            } else {
                (key, name, comments, values)
            }
        })
        .collect::<Vec<(String, String, String, String)>>();
    // remove first entry, it should be an empty entry
    data_entries.remove(0);

    //    panic!();

    let enums = data_entries
        .iter()
        .map(|(key, name, comments, values)| {
            to_code(
                key.to_string(),
                name.to_string(),
                comments.to_string(),
                values.to_string(),
            )
        })
        .collect::<Vec<(String, String)>>();
    //println!("{}", enums.join("\n"));

    enums
}

fn process_enums_inner_data(
    key: String,
    name: String,
    comments: String,
    values: String,
) -> (String, String, String, String) {
    // Special handling of 'SummaryInventory'
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY k {}", key);
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY n {}", name);
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY c {}", comments);
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY v {}", values);
    if key.eq("charging") || key.eq("For") || key.eq("-") {
        println!("XXXXXXXXXXXXXXXXXXXXXX");
        ("".to_owned(), "".to_owned(), "".to_owned(), "".to_owned())
    } else {
        (key, name, comments, values)
    }
}

fn process_datatypes_inner_data(
    key: String,
    name: String,
    comments: String,
    values: String,
) -> (String, String, String, String) {
    // Special handling of 'SummaryInventory'
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY k {}", key);
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY n {}", name);
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY c {}", comments);
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY v {}", values);
    if key.eq("charging") || key.eq("For") || key.eq("-") {
        println!("XXXXXXXXXXXXXXXXXXXXXX");
        ("".to_owned(), "".to_owned(), "".to_owned(), "".to_owned())
    } else {
        (key, name, comments, values)
    }
}

fn process_messages_inner_data(
    key: String,
    name: String,
    comments: String,
    values: String,
) -> (String, String, String, String) {
    // Special handling of 'SummaryInventory'
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY k {}", key);
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY n {}", name);
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY c {}", comments);
    println!("YYYYYYYYYYYYYYYYYYYYYYYYYY v {}", values);
    if key.eq("charging") || key.eq("For") || key.eq("-") {
        println!("XXXXXXXXXXXXXXXXXXXXXX");
        ("".to_owned(), "".to_owned(), "".to_owned(), "".to_owned())
    } else {
        (key, name, comments, values)
    }
}

fn enums_to_code(key: String, name: String, comments: String, values: String) -> (String, String) {
    (
        name.to_owned(),
        format!(
            "/* {} */\npub enum {} {{\n{}\n{}}}\n",
            key, name, comments, values
        ),
    )
}

fn datatypes_to_code(
    key: String,
    name: String,
    comments: String,
    values: String,
) -> (String, String) {
    (
        name.to_owned(),
        format!(
            "/* {} */\npub struct {} {{\n{}\n{}}}\n",
            key, name, comments, values
        ),
    )
}

fn messages_to_code(
    key: String,
    name: String,
    comments: String,
    values: String,
) -> (String, String) {
    (
        name.to_owned(),
        format!(
            "/* {} */\npub enum {} {{\n{}\n{}}}\n",
            key, name, comments, values
        ),
    )
}

fn enums_map_attribute(attribute: &str) -> String {
    // Special handling of 'SummaryInventory'
    // charging, For and -
    if attribute.eq("")
        || attribute.starts_with("charging")
        || attribute.starts_with("For")
        || attribute.starts_with("-")
    {
        format!("")
    } else {
        let (attribute, comment) = attribute.split_once(" ").unwrap_or((&attribute, ""));
        let key = attribute.replace("-", "_").replace(".", "_");
        format!("\t{}, // {}", key, comment)
        // attribute can be used as the string representation of the enum in json
        // todo generate impl block for enum or serde annotations
    }
}

fn datatypes_map_attribute(attribute: &str) -> String {
    // Special handling of 'SummaryInventory'
    // charging, For and -
    if attribute.eq("") || attribute.eq("Field Name Field Type Card. Description") {
        format!("")
    } else {
        let (field_name, remaining) = attribute.split_once(" ").unwrap_or((&attribute, ""));
        let remaining = remaining.replace("integer, 0 < = val < = 100", "integer[0..100]");
        let remaining = remaining.replace("integer, 0 < = val", "integer[0..]");
        let (field_type, remaining) = remaining.split_once(" ").unwrap_or((&remaining, ""));
        let (cardinality, description) = remaining.split_once(" ").unwrap_or((&remaining, ""));
        let field_name = field_name.replace("-", "_").replace(".", "_");
        let field_name = if field_name.eq("type") {
            "r#type".to_owned()
        } else {
            field_name
        };
        let mut field_type = field_type
            .replace("[", "")
            .replace("..", "_")
            .replace("]", "");
        let field_type = if !field_type.is_empty() {
            field_type.remove(0).to_uppercase().to_string() + &field_type
        } else {
            field_type
        };
        format!(
            "\t{}: {}, // {} {}",
            field_name, field_type, cardinality, description
        )
    }
}

fn messages_map_attribute(attribute: &str) -> String {
    // Special handling of 'SummaryInventory'
    // charging, For and -
    if attribute.eq("")
        || attribute.starts_with("charging")
        || attribute.starts_with("For")
        || attribute.starts_with("-")
    {
        format!("")
    } else {
        let (attribute, comment) = attribute.split_once(" ").unwrap_or((&attribute, ""));
        let key = attribute.replace("-", "_").replace(".", "_");
        format!("\t{} = \"{}\", // {}", key, attribute, comment)
    }
}

fn primitive_datatypes() -> String {
    "// string The characters defined in the UTF-8 character set are allowed to be used.
    type String0_8 = String;
    type String0_16 = String;
    type String0_20 = String;
    type String0_25 = String;
    type String0_32 = String;
    type String0_50 = String;
    type String0_128 = String;
    type String0_255 = String;
    type String0_512 = String;
    type String0_800 = String;
    type String0_1000 = String;
    type String0_2500 = String;
    type String0_5500 = String;

    // integer 32 bit (31 bit resolution, 1 sign bit)
    // No leading 0’s
    // No plus sign
    // Allowed value examples: 1234, -1234
    // Not Allowed: 01234, +1234    
    type Integer = i32;
    type Integer0_100 = i32;
    type Integer0_ = i32;
    
    // decimal For data being reported by the Charging Station, the full resolution of the source data must be
    // preserved. The decimal sent towards the Charging Station SHALL NOT have more than six
    // decimal places.
    type Decimal = f64;

    // identifierString This is a case-insensitive dataType and can only contain characters from the following
    // character set: a-z, A-Z, 0-9, '*', '-', '_', '=', ':', '+', '|', '@', '.'
    type IdentifierString = String;
    type IdentifierString0_6 = String;
    type IdentifierString0_20 = String;
    type IdentifierString0_36 = String;
    type IdentifierString0_40 = String;
    type IdentifierString0_50 = String;
    type IdentifierString0_128 = String;

    // dateTime All time values exchanged between CSMS and Charging Station SHALL be formatted as
    // defined in [RFC3339]. Additionally fractional seconds have been given an extra limit. The
    // number of decimal places SHALL NOT exceed the maximum of 3.
    // Example 1: 2019-04-12T23:20:50.52Z represents 20 minutes and 50.52 seconds after the 23rd
    // hour of April 12th, 2019 in UTC.
    // Example 2: 2019-12-19T16:39:57+01:00 represents 39 minutes and 57 seconds after the 16th
    // hour of December 19th, 2019 with an offset of +01:00 from UTC (Central European Time).
    type DateTime = String;

    // AnyType Text, data without specified length or format.

    // boolean Only allowed values: \"false\" and \"true\".
    type Boolean = bool;
\n"
        .to_string()
}
